#include "inintdb.h"
#include "dbconnection.h"
/**
 * @brief InitInventoryDB - создает БД и заполняет данными
 * @return - ошибка выполнения sql запросов
 */
QSqlError InitDB(){
    DBConnection db;

    QString Table = "CREATE TABLE Person (Name     CHAR (255),LastName CHAR (255),Email    CHAR (255),Age      INT,Kids     BOOLEAN,Likes    BLOB,Actions);";

    db.exec(Table);

return db.lastError();
}
