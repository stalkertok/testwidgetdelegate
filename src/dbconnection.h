#ifndef DBCONNECTION_H
#define DBCONNECTION_H

#include <QSqlDatabase>
#include <QSqlQuery>
#include <QSqlError>
#include <QString>

const QString dbN = "DB.db";

/**
 * @brief The DBConnection class - Класс для выполнения sql запросов, создания соединения с БД. По умолчанию создается БД dbInventory
 */
class DBConnection{

public:
    DBConnection(const QString& dbName = dbN);
    ~DBConnection();
    bool open();
    bool isOpen()const;
    void close();
    QSqlQuery exec(const QString& queryString);
    QSqlError lastError() const;
    QSqlDatabase& DB();

private:
    QSqlDatabase db;
};

#endif // DBCONNECTION_H
