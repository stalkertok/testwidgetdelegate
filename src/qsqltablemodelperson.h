#ifndef QSQLTABLEMODELPERSON_H
#define QSQLTABLEMODELPERSON_H

#include <QSqlTableModel>


class QSqlTableModelPerson : public QSqlTableModel
{
public:
    QSqlTableModelPerson();
    QVariant data ( const QModelIndex & index, int role = Qt::DisplayRole ) const;
    Qt::ItemFlags flags ( const QModelIndex & index ) const;
    bool setData(const QModelIndex &index, const QVariant &value, int role);

};

#endif // QSQLTABLEMODELPERSON_H
