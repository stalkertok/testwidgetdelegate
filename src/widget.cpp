#include "widget.h"
#include "ui_widget.h"
#include "inintdb.h"
#include <QDebug>
#include <QCheckBox>
#include "qsqltablemodelperson.h"
#include <QFileDialog>
#include "qcheckboxdelegate.h"
#include "qspinboxdelegate.h"
#include <QStandardItem>


Widget::Widget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Widget)
{
    ui->setupUi(this);

    InitDB();

        model=new QSqlTableModelPerson();
        model->setTable(TableName);
        model->setEditStrategy(QSqlTableModel::OnManualSubmit);
        model->select();
        ui->tableView->setModel(model);
       // ui->tableView->setItemDelegateForColumn(4,new CheckBoxDelegate(this));
        ui->tableView->setItemDelegateForColumn(3,new SpinBoxDelegate(this));
}

Widget::~Widget()
{
    InitDB();
    submitAll();
    delete ui;
    delete  model;
}

void Widget::on_AddPushButton_clicked(){
    model-> insertRow(model->rowCount());
}


void Widget::submitAll(){
   DBConnection db;
   model->submitAll();
}

void Widget::on_removePushButton_clicked(){
    model->removeRow(ui->tableView->currentIndex().row());
}

void Widget::on_tableView_doubleClicked(const QModelIndex &index){
//    if (index.column()==5){
//        QFileDialog d;
//        if (d.exec()){
//        QPixmap pixmap(d.getOpenFileName());
//        // Preparation of our QPixmap
//        QByteArray bArray;
//        QBuffer buffer(&bArray);
//        buffer.open(QIODevice::WriteOnly);
//        pixmap.save(&buffer, "PNG");
//        model->setData(index, );
//    }
//    }

}

void Widget::on_tableView_clicked(const QModelIndex &index)
{
    ui->tableView->doubleClicked(index);
}
