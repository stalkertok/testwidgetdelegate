#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>
#include <QSqlTableModel>
#include <QSqlRecord>
#include <dbconnection.h>

namespace Ui {
class Widget;
}

const QString TableName = "Person";

class Widget : public QWidget
{
    Q_OBJECT

public:
    explicit Widget(QWidget *parent = 0);
    ~Widget();

private slots:
    void on_AddPushButton_clicked();
    void submitAll();

    void on_removePushButton_clicked();

    void on_tableView_doubleClicked(const QModelIndex &index);

    void on_tableView_clicked(const QModelIndex &index);

private:
    Ui::Widget *ui;
    QSqlTableModel *model;

};

#endif // WIDGET_H
