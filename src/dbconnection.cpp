#include "dbconnection.h"

DBConnection::DBConnection(const QString &dbName){
    if(QSqlDatabase::contains(QSqlDatabase::defaultConnection)) {
        db = QSqlDatabase::database();
    } else {
        db = QSqlDatabase::addDatabase("QSQLITE");
        db.setDatabaseName(dbName);
    }

    db.open();
}

DBConnection::~DBConnection(){
    this->close();
}

bool DBConnection::open(){

    return db.open();
}

bool DBConnection::isOpen()const{
    return db.isOpen();
}

void DBConnection::close(){
    db.close();
}

QSqlQuery DBConnection::exec(const QString &query){
    return db.exec(query);
}

QSqlError DBConnection::lastError() const{
    return db.lastError();
}

QSqlDatabase& DBConnection::DB(){
return db;

}
