#include "qsqltablemodelperson.h"
#include <QDebug>

QSqlTableModelPerson::QSqlTableModelPerson()
{

}

QVariant QSqlTableModelPerson::data(const QModelIndex &index, int role) const{
    auto value= QSqlTableModel::data(index,role);

    //if (role == Qt::DisplayRole && index.column()==4) return QVariant();

    if (role == Qt::CheckStateRole && index.column()==4){
        if (index.data().toBool())
            return Qt::Checked;
        else
            return Qt::Unchecked;
    }

    return value;
}

Qt::ItemFlags QSqlTableModelPerson::flags ( const QModelIndex & index ) const{
    Qt::ItemFlags flags = QSqlQueryModel::flags(index);
    if (index.column() == 4){
        flags=Qt::ItemIsUserCheckable;
        flags|=Qt::ItemIsEnabled;
    }
     else
         flags|=Qt::ItemIsEditable;

    return flags;
}

bool QSqlTableModelPerson::setData(const QModelIndex &index, const QVariant &value, int role)
{
    if (!index.isValid())
        return false;
    if (role == Qt::CheckStateRole & index.column()==4)
    {
        if ((Qt::CheckState)value.toInt() == Qt::Checked)
        {
            QSqlTableModel::setData(index,true,role);
            auto i=index.data().toBool();
            qDebug()<<i;
            return true;
        }
        else
        {
            QSqlTableModel::setData(index,false,role);
            auto i=index.data().toBool();
            qDebug()<<i;
            return true;
        }
    }
    return QSqlTableModel::setData(index,value,role);
}



