#include "qcheckboxdelegate.h"
#include <QCheckBox>

CheckBoxDelegate::CheckBoxDelegate(QObject *parent)
    : QItemDelegate(parent)
{
}

QWidget *CheckBoxDelegate::createEditor(QWidget *parent,
    const QStyleOptionViewItem &,
    const QModelIndex &) const
{
    QCheckBox *editor = new QCheckBox(parent);

    return editor;
}

void CheckBoxDelegate::setEditorData(QWidget *editor,
                                    const QModelIndex &index) const
{
    bool value = index.model()->data(index, Qt::EditRole).toBool();

    QCheckBox *checkBox = static_cast<QCheckBox*>(editor);
    if (value)
        checkBox->setCheckState(Qt::CheckState::Checked);
    else
        checkBox->setCheckState(Qt::CheckState::Unchecked);

}

void CheckBoxDelegate::setModelData(QWidget *editor, QAbstractItemModel *model,
                                   const QModelIndex &index) const
{
    QCheckBox *checkBox = static_cast<QCheckBox*>(editor);

    if (checkBox->checkState()==Qt::CheckState::Checked)
    model->setData(index, true, Qt::EditRole);
            else
    model->setData(index, false, Qt::EditRole);
}

void CheckBoxDelegate::paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const
{
    drawDisplay(painter,option,option.rect,index.model()->data( index, Qt::DisplayRole ).toBool()?QString("      ").append(tr("Yes")):QString("      ").append(tr("No")));
    drawFocus(painter,option,option.rect);
}

void CheckBoxDelegate::updateEditorGeometry(QWidget *editor,
    const QStyleOptionViewItem &option, const QModelIndex &/* index */) const
{
    editor->setGeometry(option.rect);
}
