/********************************************************************************
** Form generated from reading UI file 'widget.ui'
**
** Created by: Qt User Interface Compiler version 5.12.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_WIDGET_H
#define UI_WIDGET_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QTableView>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_Widget
{
public:
    QGridLayout *gridLayout;
    QLineEdit *lineEdit;
    QPushButton *SendpushButton;
    QPushButton *AddPushButton;
    QPushButton *removePushButton;
    QTableView *tableView;

    void setupUi(QWidget *Widget)
    {
        if (Widget->objectName().isEmpty())
            Widget->setObjectName(QString::fromUtf8("Widget"));
        Widget->resize(400, 300);
        gridLayout = new QGridLayout(Widget);
        gridLayout->setSpacing(6);
        gridLayout->setContentsMargins(11, 11, 11, 11);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        lineEdit = new QLineEdit(Widget);
        lineEdit->setObjectName(QString::fromUtf8("lineEdit"));

        gridLayout->addWidget(lineEdit, 2, 0, 1, 1);

        SendpushButton = new QPushButton(Widget);
        SendpushButton->setObjectName(QString::fromUtf8("SendpushButton"));

        gridLayout->addWidget(SendpushButton, 2, 1, 1, 1);

        AddPushButton = new QPushButton(Widget);
        AddPushButton->setObjectName(QString::fromUtf8("AddPushButton"));

        gridLayout->addWidget(AddPushButton, 1, 1, 1, 1);

        removePushButton = new QPushButton(Widget);
        removePushButton->setObjectName(QString::fromUtf8("removePushButton"));

        gridLayout->addWidget(removePushButton, 1, 2, 1, 1);

        tableView = new QTableView(Widget);
        tableView->setObjectName(QString::fromUtf8("tableView"));
        tableView->setDragDropMode(QAbstractItemView::NoDragDrop);
        tableView->setSortingEnabled(false);

        gridLayout->addWidget(tableView, 0, 0, 1, 3);


        retranslateUi(Widget);

        QMetaObject::connectSlotsByName(Widget);
    } // setupUi

    void retranslateUi(QWidget *Widget)
    {
        Widget->setWindowTitle(QApplication::translate("Widget", "Widget", nullptr));
        SendpushButton->setText(QApplication::translate("Widget", "Send", nullptr));
        AddPushButton->setText(QApplication::translate("Widget", "Add", nullptr));
        removePushButton->setText(QApplication::translate("Widget", "Remove", nullptr));
    } // retranslateUi

};

namespace Ui {
    class Widget: public Ui_Widget {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_WIDGET_H
